Börje

https://arxiv.org/pdf/1606.01541.pdf https://arxiv.org/pdf/1706.03850.pdf http://aclweb.org/anthology/D17-1065

BiGAN https://arxiv.org/pdf/1605.09782v7.pdf

https://arxiv.org/pdf/1505.07818.pdf 这个是对应第一个idea

- **Attention mechanism**: 
Dzmitry Bahdanau, Kyunghyun Cho, and Yoshua Bengio. Neural machine translation by jointly learning to align and translate. In Proceedings of the 2014 International Conference on Learning Representations, 2014.


问题：
- negative samplig 到底是怎么实现的？ Unigram noise distribution 是什么？如何根据它采样？


#### word embedding
- ==2016 Enriching Word Vectors with Subword Information==
    - Piotr Bojanowski, Tomas Mikolov
    - Facebook AI Research - arXiv
    - 概要：本文首先分析了negative sampling model存在的缺陷：没有考虑word中包含的 subword 信息，本文通过 n-gram 的 bag 来表示一个 word，从而引入subword 信息，从而能够：在不同words 之间 share representation；rare words的repr.也更加可靠；不存在于 vocabulary 的 words 也能够通过 sum of repr. of its n-grams 的方式得到它的 repr. 由于引入 n-gram，词素急剧上升，所以本文又引入了 hash map 的方式来降低对存储空间的要求。
    - 对本文的评价：**大框架了解了，但有很多细节没有看懂，例如每个词素的 向量如何得到的？hash map 是怎么用的？模型的具体结构是？……**
- ==2013 Distributed representations of words and phrases and their compositionality==
    - Tomas Mikolov, Jeffrey Dean
    - Google - In adv. NIPS
    - 概要：本文指出了作者前一篇文章中所以 CBOW model 与 skip-gram model 的不足：indifference to word order & unable to represent idiomatic phrases。并在前一篇 skim-gram model 的基础上提出了两种改进的 WE 方法：hierarchical softmax skip gram model 与 negtive sampling model。其中，hierarchical soft 采用 huffman binary tree 代替 output layer，利用树形结构将复杂度降至 O(log(W))；Negative sampling以在由噪声分布中采样得到的负样本中分辨出 target word 的思想，进一步降低了复杂度，使得**在 large corpus 上的训练变得十分高效**。为了降低 frequent words 与 rare words 样本的不平衡性，本文还介绍了 subsampling of frequent words 的方法：以一定概率忽略高频样本，在以上三种 WE 方法中都取得了很好的效果。
- ==2013 Efficient estimation of word representations in vector space==
    - Tomas Mikolov, Jeffrey Dean
    - Google - arXiv
    - 概要：首先介绍了两种 existed word embedding 方法：Feedforward NNLM 和 RNNLM；然后介绍了本文提出的两种模型：CBOW 和 skip gram。优势：计算复杂度更低；在semantic 和 syntactic task上的准确度更高。
- 【待看】2014 Notes on noise contrastive estimation and negative sampling
    - Dyer, Chris
    - CMU - arXiv
- 【待看】2014 word2vec Explained: deriving Mikolov et al.'s negative-sampling word-embedding method
    - Goldberg, Yoav, and Omer Levy.
    - arXiv
- 【待看】[word2vec 原理推导与代码](http://www.hankcs.com/nlp/word2vec.html)


#### Translation
- ==2018.8.31 Unsupervised machine translation: A novel approach to provide fast, accurate translations for more languages==
    - Marc’Aurelio Ranzato, Guillaume Lample, Myle Ott
    - Facebook - AI Research
    - 概要：在[上篇文章](https://arxiv.org/abs/1710.04087)的基础上，进一步提升了 unsupervised translation 的性能
        - STEP 1: word-by-word trannslation
        - STEP 2: translating sentence
            - STEP 2-2: MT system (Version 1): the language model + word-by-word initialization (Urdu -> English)
            - STEP 2-3: MT system (Version 2): back translation (back translation)
    - 对文本的评价：本文来自于网页，其中对迭代改进模型效率的描述不足，也没有具体的实验结果。
- ==2018.4 Unsupervised machine translation using monolingual corpora only==
    - Guillaume Lample, Marc'Aurelio Ranzato 
    - Facebook - ICLR2018
    - 概要： use a single encoder and a sigle decoder
        - STEP 1: Word-to-word translation.
        - STEP 2: Train the encoder-decoder at each iteration, reconstruct task & translate task, given the noisy input.
        - STEP 3 (Simultaneously): Align the laten distribution of sentences in both domains, by learning a discriminator in an adversarial setting.
- ==2018.2 Unsupervised Nueral Machine Translation== 
    - Mikel Artetxe, Kyunghyum Cho
    - UPV/EHU - ICLR 2018
    - 概要：for each stc. in **lang. L1**, train allternating in 2 steps:
        - STEP 1: **Denoising**: shared encoder + L1 decoder
            - random swap
        - STEP 2: **On-the-fly Backtranslation**, including 2 parts 
            - PART 1: translate in inference mode: shared encoder + L2 decoder
            - PART 2: for the translated stc.: shared encoder + L1 decoder
- ==2018.1 Word Translation Without Parallel Data==
    - Alexis Conneau, Guillaume Lample, Marc’Aurelio Ranzato
    - Facebook - ICLR 2018
    - 概要：本文的目的在于不借用任何 cross-lingual supervision 手段进行 word translation。
        - STEP 1: 利用 adversarial leaning 得到一个 initial proxy of W.
        - STEP 2: 通过不断迭代的方式，找到最终的W。最终的模型通过一个 unsupervised validation metric 获得。
            - STEP 2-1: 考虑 most freq. words, 应用 CSLS similarity metric 求 only mutual nearest heighbors => a relative high quality dict.
            - STEP 2-2: 利用上述 dict. 求解 Procrustes Problem.
    - 对本文的评价：本文首次提出了不借用任何 cross-lingual supervision 手段进行 word embeddings alignment的方法——adversarial learning. 本文的创新点在两处：提出了 CSLS similarity metric，expand 了WE密集区；提出了unsupervised validation metric，以选择不断更新W时的最优模型。

#### NER
- ==2018.9 Neural Cross-Lingual Named Entity Recognition==
    - Jiateng Xie
    - CMU - arXiv
    - 概要：本文提出了两种方法来解决 under the unsupervised transfer setting 下 cross-lingual NER 中的挑战。lexical mapping (STEP 1-3). word ordering (STEP 4).
        - STEP 1: 用 monolingual corpora 各自训练不同语种的 WE
        - STEP 2: 通过下述步骤的不断迭代，得到最终的 translations，并copy labels directly
            - STEP 2-1: Proscutes Problem：用 seed dictionary 优化 WE alignment，将不同语种的 WE 映射到 a shared embedding space。
            - STEP 2-2: 在 shared space 中利用 CSLS similarity metric 对 source lang 进行翻译
        - STEP 3: 利用由STEP 2 得到的training data 和 labels train an NER model，引入了 self-attention layer.
    - 对本文的评价：创新性实则在于引入 self-attention layer，其对于 lexical mapping 问题（即source lang. translation的问题）的解决并没有创新，方法同 Word Translation Without Parallel Data, Alexis Conneau, 2018.1 的完全一致。
- ==2017 Cheap translation for cross-lingual named entity recognition==
    - Mayhew
    - Illinoise Champaign - Conference on Empirical Methods in NLP
    - 概要：
        - 提出了一个生成翻译字典的 cheap translation 算法
        - 该算法可以和 wikifier features、Brown Cluster features  等结合取得更好的效果
        - 通过实验说明当 source Lan. 与 traget Lan. 相似度比较高、特别是target 语言的一词多义/与其他词语的组合引起的含义变化比较多时，可以进一步提高模型性能。
        - 探究了 cheap translation 所需字典的size，及其他一些 lan. specific 的 techniques 对模型性能的影响
    - 对本文的评价：本文的创新点在于重点关注了**一词多义**的情况，通过与其他词一起出现的频率定义了 prominence score，构建了phrase translation table，利用 greedy decoding  method 进行翻译。